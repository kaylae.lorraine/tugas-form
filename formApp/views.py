from django.shortcuts import render,redirect
from .models import Person
from .forms import PersonForm, searchForm

# Create your views here.
def addPerson(request):
    if request.method == "POST":
        form = PersonForm(request.POST)
        if form.is_valid():
            nambah_orang = Person()
            nambah_orang.Name = form.cleaned_data['Name']
            nambah_orang.Date = form.cleaned_data['Date']
            nambah_orang.Age = form.cleaned_data['Age']
            nambah_orang.Food = form.cleaned_data['Food']
            nambah_orang.Color = form.cleaned_data['Color']
            nambah_orang.save()
            all_orang = Person.objects.all()
            orang_form = PersonForm()
            form1 = searchForm()
            res = {'all_orang' : all_orang, 'orang_form' : orang_form ,'form':form1}
        return render (request, 'home.html', res)
    else:
        nambah_orang = Person.objects.all()
        orang_form = PersonForm()
        form1 = searchForm()
        res = {'nambah_orang' : nambah_orang, 'orang_form' : orang_form,'form':form1}
        return render (request, 'home.html', res)

def filterPerson(request):
    if request.method == "POST":
        form = searchForm(request.POST)
        if form.is_valid():
            orang_form = PersonForm()
            cari = form.cleaned_data['searchForm']
            if cari=='all':
                search = Person.objects.all()
            else :
                search = Person.objects.filter(Name=cari)
            form = searchForm()
            res = {'form' : form, 'nambah_orang':search, 'orang_form':orang_form}
        return render (request, 'cari-orang.html', res)
    else:
        # search = Person.objects.filter(Name=cari)
        form = searchForm()
        res = {'form' : form}
        return render (request, 'cari-orang.html', res)