from django.db import models

# Create your models here.
class Person(models.Model):
    Name = models.CharField(max_length=20)
    Date = models.DateField(blank=False)
    Age = models.IntegerField()
    Food = models.CharField(max_length=20)
    Color = models.CharField(max_length=20)
    def __str__(self):
        return"{}".format(self.Name)
