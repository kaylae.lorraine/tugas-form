from django.urls import path,include
from .views import addPerson,filterPerson
urlpatterns = [
    path('', addPerson, name = 'home'),
    path('search/', filterPerson, name = 'search'),
]