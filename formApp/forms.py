from django import forms

class PersonForm(forms.Form):

    Name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama',
        'type' : 'text',
        'required' : True,
    }))

    Date = forms.DateField(widget=forms.DateInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'dd/mm/yyyy',
        'type' : 'date',
        'required' : True,
    }))

    Age = forms.IntegerField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Age',
        'type' : 'int',
        'required' : True,
    }))

    Food = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Favourite Food',
        'type' : 'text',
        'required' : True,
    }))

    Color = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Favourite Color',
        'type' : 'text',
        'required' : True,
    }))

class searchForm(forms.Form):
    searchForm = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Name',
        'type' : 'text',
        'required' : True,
    }))
